import json
from collections import defaultdict, OrderedDict
from bs4 import BeautifulSoup
import os
import xlsxwriter


def fill_xls(*, new_data):
    workbook = xlsxwriter.Workbook('demo.xlsx')
    worksheet = workbook.add_worksheet()
    bold = workbook.add_format({'bold': True})
    italic = workbook.add_format({'italic': True})
    print(new_data)
    max_row = 1
    counter = 1
    for cell in database:
        column = 1
        row = max_row
        """Position Name and number"""
        worksheet.set_column(row, 0, 10)
        worksheet.write(row, 0, counter, bold)
        worksheet.set_column(row, column, 100)
        worksheet.write(row, column, cell, bold)
        print(cell)
        """Basic Parameters"""
        for inner_key, inner_value in new_data[f'{cell}'].items():
            print(inner_key," ", inner_value)
            column += 1
            row = max_row
            worksheet.write(row, column, inner_key+':', bold)
            if inner_key != 'Produktdetails':
                for item in inner_value:
                    row += 1
                    worksheet.set_column(row, column, 40)
                    worksheet.write(row, column, item, italic)
            else:
                for item in inner_value :
                    for key, value in item.items():
                        if value == "":
                            out = f"{key}: None"
                        else:
                            out = f"{key}: {value}"
                        max_row += 1
                        row += 1
                        worksheet.set_column(row, column, 40)
                        worksheet.write(row, column, out, italic)
        max_row += 2
        counter += 1
    workbook.close()





i = 0
database = dict()
while os.path.exists(f"URL_good{i}.html"):
    test_dict = defaultdict(list)
    with open(f"URL_good{i}.html", 'r', encoding='utf-8') as f:
        data = BeautifulSoup(f.read(), "html.parser")
        key = (data.find("title").get_text()).strip()
        headers = list(map(lambda x: x.get_text().replace("\xa0", " "), data.find_all(class_="dim-heading")))
        tags = [item.attrs["id"] for item in data.find_all(class_=f"product-attribute-value")]
        new_set = list(OrderedDict.fromkeys([(item.attrs["class"][0]) for item \
                                             in data.find(id="productVariants").find_all("li")]))
        for header, label in zip(headers, new_set):
            test_dict[f"{header}"] = [item.find(class_="value").get_text() for \
                                      item in data.find_all(class_=f"{label}")]
        test_dict["Produktdetails"] = [{data.find(id=f"{item}").find_parent().get_text().split()[0]: \
                                            data.find(id=f"{item}").get_text().replace("\n", "") for item in tags}]
        database[key] = test_dict
    i += 1
fill_xls(new_data=database)

"""          Description      """


# help(BeautifulSoup)
#
# for number in range(0, 6, 1):
#     URL = f"https://shop.kloeckner.de/Aluminium/c/70000?q=%3Aname-asc&page={number}"
#     response = requests.get(f"{URL}")
#     data = response.text
#     parser_data = BeautifulSoup(response.text,"html.parser")
#     quotes = parser_data.find_all(id="resultsList")
#     print(quotes)

# """    for quote in quotes:
#         parsed_data = quote.find(class_="product-info")
#         with open("parsed_data.txt", 'a', encoding="utf-8") as f:
#             f.write(parsed_data.text)"""
#
# # csv_rows = []
# # csv_rows.append(",".join([
# #     "name",
# #     "average_position_price",
# #     "currency",
# #     "balance",
# #     "expected_yield",
# #     "ticker"
# # ]))
# #
# # with open("new_file.csv", "w") as f:
# #     f.write("\n".join(csv_rows))
